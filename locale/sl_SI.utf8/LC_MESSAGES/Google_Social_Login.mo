��    
      l      �       �   	   �      �   3     3   C  "   w     �     �     �  3   �  
  �  
          ?   !  H   a  &   �     �     �     �  (                            
             	       Client ID Google Social Login Google Social Login: Failed to get access token. %s Google Social Login: Failed to get user details. %s Google Social Login: Invalid state Hosted Domain Login with %s Redirect URI The "%s" login provider is not properly configured. Project-Id-Version: Google Social Login plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:07+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 ID Klienta Google Social Login Google Social Login: pridobitev žetona za dostop ni uspela. %s Google Social Login: Podatkov o uporabniku ni bilo mogoče pridobiti. %s Google Social Login: Neveljavno stanje Gostujoča domena Prijava z %s URI preusmeritve Ponudnik  "%s" ni pravilno konfiguriran. 