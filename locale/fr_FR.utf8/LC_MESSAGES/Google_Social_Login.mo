��          |      �          	   !     +     9     M  3   e  3   �  "   �     �     �       3     
  M     X     e     v     �  E   �  Q   �  #   :     ^     p       6   �                                          
          	       Client ID Client Secret Google Social Login Google Social Login: %s Google Social Login: Failed to get access token. %s Google Social Login: Failed to get user details. %s Google Social Login: Invalid state Hosted Domain Login with %s Redirect URI The "%s" login provider is not properly configured. Project-Id-Version: Google Social Login plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:07+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 ID du client Secret du client Google Social Login Google Social Login: %s Google Social Login: erreur lors de l'obtention du jeton d'accès. %s Google Social Login: erreur lors de l'obtention des détails de l'utilisateur. %s Google Social Login: état invalide Domaine hébergé Entrer avec %s URI de redirection Le fournisseur de connexion "%s" n'est pas configuré. 