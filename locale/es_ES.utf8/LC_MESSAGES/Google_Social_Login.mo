��          |      �          	   !     +     9     M  3   e  3   �  "   �     �     �       3     <  M     �     �     �     �  <   �  ?     $   T     y     �     �  1   �                                          
          	       Client ID Client Secret Google Social Login Google Social Login: %s Google Social Login: Failed to get access token. %s Google Social Login: Failed to get user details. %s Google Social Login: Invalid state Hosted Domain Login with %s Redirect URI The "%s" login provider is not properly configured. Project-Id-Version: Google Social Login plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:06+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 ID de Cliente Secreto de Cliente Google Social Login Google Social Login: %s Google Social Login: Fallo en obtener un token de acceso. %s Google Social Login: Fallo en obtener los datos del usuario. %s Google Social Login: Estado invalido Dominio Alojado Entrar con %s URI de redirección El proveedor de acceso "%s" no está configurado. 