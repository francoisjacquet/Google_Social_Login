<?php
/**
 * Google Social Login Redirect
 *
 * Use as direct link to from your SSO portal
 * Copy this file to the root of your RosarioSIS installation
 * and redirect your user to the following link:
 * http://yourdomain.com/INSTALL_LOCATION/GoogleSocialLoginRedirect.php
 *
 * This will fix the "You must accept cookies to use RosarioSIS." error.
 *
 * @package Google Social Login plugin
 */

// Set the session cookie
require_once 'Warehouse.php';

$login_with_google_url = 'plugins/Google_Social_Login/provider/Google.php';

// Redirect to "Login with Google" (without the need to display the RosarioSIS login screen)
header( 'Location: ' . $login_with_google_url );
